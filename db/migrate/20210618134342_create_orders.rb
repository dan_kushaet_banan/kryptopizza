class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|

      t.references :user
      t.integer :order_price
      t.string :dest_address
      t.string :name
      t.integer :phone_number

      t.timestamps
    end
  end
end
