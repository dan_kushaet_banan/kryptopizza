class CreateOrderProduct < ActiveRecord::Migration[6.1]
  def change
    create_table :order_products do |t|

      t.references :product
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
