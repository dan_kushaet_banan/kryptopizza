class OrderReportMailer < ApplicationMailer

  def report
    @user = params.fetch(:user)
    @order = params[:order]

    mail(to: @user.email, subject: 'Your order is still accepted!')
  end

end