class RegistrationMailer < ApplicationMailer

  def report
    @user = params[:user]

    mail(to: @user.email, subject: 'Successful register')
  end

end