class CheckoutsService

  def initialize(session, order_params)
    @session = session
    @order_params = order_params
    @key = if session['warden.user.user.key']
             session['warden.user.user.key'][0][0]
           else
             session[:token]
           end
  end

  def make_order
    @basket = ::BasketsService.new(@session).all

    build_form_params
    order = Order.new(@order_params)
    return order.errors unless order.valid?

    order.save
    make_order_product
    ::EmailSenderWorker.perform_async(@session['warden.user.user.key'][0][0], @order.id) if @session['warden.user.user.key'][0][0]
  end

  private

  def build_form_params
    @order_params[:order_price] = ::BasketsService.new(@session).sum * 1.2
    @order_params[:user_id] = @key
  end

  def make_order_product
    @basket.each { |product| OrderProduct.create!(product_id: product['id'], order_id: @order.id) }
  end
end