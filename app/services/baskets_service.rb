class BasketsService
  def initialize(session)
    @session = session
    @key = if session['warden.user.user.key']
             session['warden.user.user.key'][0][0]
           else
             token
           end
    @redis_basket = Redis.current
  end

  def add(id)
    product = Product.find(id)
    products = all

    products << product
    @redis_basket.set(@key, products.to_json)
  end

  def remove(id)
    product = Product.find(id)
    products = all

    products.delete_at(products.index(product.as_json)) if products.include?(product.as_json)
    @redis_basket.set(@key, products.to_json)
  end

  def all
    if @redis_basket.exists?(@key)
      JSON.parse(@redis_basket.get(@key))
    else
      []
    end
  end

  def clear
    @redis_basket.del(@key)
  end

  def size
    if @redis_basket.exists?(@key)
      products = JSON.parse(@redis_basket.get(@key))
      @size = products.length
    else
      @size = 0
    end
  end

  def sum
    products = JSON.parse(@redis_basket.get(@key)) if @redis_basket.exists?(@key)
    sum = 0
    products&.each { |x| sum += x['price'] }
    sum
  end

  private

  attr_reader :session

  def token
    @token ||= @session[:token] || SecureRandom.hex(8)
    @session[:token] = @token
  end
end