class RegistrationEmailSenderWorker
  include Sidekiq::Worker

  def perform(user_id)
    RegistrationMailer.with(user: User.find(user_id)).report.deliver_now
  end
end
