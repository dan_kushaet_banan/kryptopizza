class EmailSenderWorker
  include Sidekiq::Worker

  def perform(user_id, order_id)
    OrderReportMailer.with(user: User.find(user_id), order: Order.find(order_id)).report.deliver_now
  end
end
