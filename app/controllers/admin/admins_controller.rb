module Admin
  class AdminsController < ApplicationController
    before_action :authenticate_user!

    def admin?
      user = current_user
      user.has_role? :admin
    end

    def authorize_admin
      redirect_to '/' unless admin?
    end
  end
end