module Admin
  class OrdersController < AdminsController
    before_action :authorize_admin

    def index
      @orders = Order.all
    end

    def show
      @order = Order.find(params[:id])
      make_products_array
    end

    def new
      @products = @basket.all
      @order = Order.new
      @user = User.find(session['warden.user.user.key'][0][0])
    end

    def create
      @order = Order.new(order_params)

      if @order.valid?
        @order.save!
        redirect_to admin_order_path(@order.id)
      else
        @products = @basket.all
        @user = User.find(session['warden.user.user.key'][0][0])
        render :new, status: :unprocessable_entity
      end

    end

    def edit
      @order = Order.find(params[:id])
      make_products_array
    end

    def update
      @order = Order.find(params[:id])

      if @order.update(order_params)
        redirect_to admin_order_path(params[:id])
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @order_products = OrderProduct.find_by(order_id: params[:id])
      @order = Order.find_by_id(params[:id])
      @order&.destroy

      redirect_to admin_orders_path
    end

    private

    def order_params
      params.require(:order).permit(:user_id, :order_price, :dest_address, :name, :phone_number)
    end

    def make_products_array
      @products = Product.joins(:order_products).where(order_products: { order_id: @order.id })
    end
  end
end