module Admin
  class ProductsController < AdminsController
    before_action :authorize_admin

    def index
      @products = Product.all
      @products_in_basket = @basket.all
    end

    def show
      @product = Product.find(params[:id])
    end

    def new
      @product = Product.new
    end

    def create
      @product = Product.new(product_params)

      if @product.valid?
        @product.save!
        redirect_to admin_product_path(@product.id)
      else
        render :new, status: :unprocessable_entity
      end

    end

    def edit
      @product = Product.find(params[:id])
    end

    def update
      @product = Product.find(params[:id])

      if @product.update(product_params)
        redirect_to admin_product_path(params[:id])
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @product = Product.find_by_id(params[:id])
      @product&.destroy
      redirect_to admin_products_path
    end

    def add_to_basket
      ::BasketsService.new(session).add(params[:id])
      @products = Product.all
      @products_in_basket = @basket.all
      redirect_to admin_products_path
    end

    def delete_from_basket
      ::BasketsService.new(session).remove(params[:id])
      @products = Product.all
      @products_in_basket = @basket.all
      redirect_to admin_products_path
    end

    private

    def product_params
      params.require(:product).permit(:name, :description, :photo, :price)
    end
  end
end