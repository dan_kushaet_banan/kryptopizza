module Admin
  class RolesController < AdminsController
    before_action :authorize_admin

    def index
      @roles = Role.all
    end

    def show
      @role = Role.find(params[:id])
    end

    def new
      @role = Role.new
    end

    def create
      @role = Role.new(role_params)

      if @role.valid?
        @role.save!
        redirect_to admin_role_path(@role.id)
      else
        render :new, status: :unprocessable_entity
      end

    end

    def edit
      @role = Role.find(params[:id])
    end

    def update
      @role = Role.find(params[:id])

      if @role.update(role_params)
        redirect_to admin_role_path(params[:id])
      else
        @role = Role.find(params[:id])
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @role = Role.find_by_id(params[:id])
      @role&.destroy
      redirect_to admin_roles_path
    end

    private

    def role_params
      params.require(:role).permit(:name)
    end
  end
end