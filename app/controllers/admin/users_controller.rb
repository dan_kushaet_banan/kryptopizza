module Admin
  class UsersController < AdminsController
    before_action :authorize_admin

    def index
      @users = User.all
    end

    def show
      @user = User.find(params[:id])
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)

      if @user.valid?
        @user.save!
        redirect_to admin_user_path(@user.id)
      else
        render :new, status: :unprocessable_entity
      end

    end

    def edit
      @user = User.find(params[:id])
    end

    def update
      @user = User.find(params[:id])

      @user.paper_trail_event = 'update'
      if @user.update(user_params)
        redirect_to admin_user_path(params[:id])
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @user = User.find_by_id(params[:id])
      @user&.destroy
      redirect_to admin_users_path
    end

    private

    def user_params
      params.require(:user).permit(:name, :password, :email, :password_confirmation, :phone_number)
    end
  end
end
