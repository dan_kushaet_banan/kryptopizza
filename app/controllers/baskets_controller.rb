class BasketsController < ApplicationController

  def index
    @products = basket.all
  end

  def add
    ::BasketsService.new(session).add(params[:id])
    @products = basket.all
    redirect_to basket_path
  end

  def remove
    ::BasketsService.new(session).remove(params[:id])
    @products = basket.all
    redirect_to basket_path
  end

  def clear
    ::BasketsService.new(session).clear
    @products = basket.all
    redirect_to basket_path
  end

end
