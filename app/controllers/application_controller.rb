require_dependency '../services/baskets_service.rb'
require_dependency '../services/checkouts_service.rb'

class ApplicationController < ActionController::Base
  before_action :basket
  before_action :authorize?

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :phone_number, :password, :password_confirmation)}
  end

  def current_user?(user)
    current_user.id == user.id
  end

  def token
    @token ||= session[:token] || SecureRandom.hex(8)
    session[:token] = @token
  end

  def authorize
    redirect_to '/login' unless current_user
  end

  def authorize?
    token unless current_user
  end

  def basket
    @basket ||= ::BasketsService.new(session)
  end

  def redis_basket
    @redis_basket ||= Redis.current
  end

  def after_sign_in_path_for(resource)
    root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end
end
