class ProductsController < ApplicationController

  def index
    @products = Product.all
    @products_in_basket = basket.all
  end

  def show
    @product = Product.find(params[:id])
  end

  def add_to_basket
    ::BasketsService.new(session).add(params[:id])
    @products = Product.all
    @products_in_basket = basket.all
    redirect_to root_path
  end

  def delete_from_basket
    ::BasketsService.new(session).remove(params[:id])
    @products = Product.all
    @products_in_basket = basket.all
    redirect_to root_path
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :photo, :price)
  end
end