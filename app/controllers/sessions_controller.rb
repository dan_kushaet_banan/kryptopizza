class SessionsController < Devise::SessionsController

  def create
    super
    redis_basket.rename(token, session['warden.user.user.key'][0][0]) if redis_basket.exists?(token)
  end

  def destroy
    if redis_basket.exists?(session['warden.user.user.key'][0][0])
      redis_basket.rename(session['warden.user.user.key'][0][0], token)
    end
    super
  end
end