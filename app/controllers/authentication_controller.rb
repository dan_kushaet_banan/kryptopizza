class AuthenticationController < ApplicationController

  def new
    @user = User.new
  end

  def create
    @user = User.find_by(email: user_params[:email])

    if @user&.authenticate(user_params[:password])
      session['warden.user.user.key'][0][0] = @user.id
      redis_basket.rename(token, session['warden.user.user.key'][0][0]) if redis_basket.exists?(token)

      redirect_to admin_user_path(@user.id)
    else
      @error_message = 'Invalid email/password combination'
      @user = User.new
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    redis_basket.rename(session['warden.user.user.key'][0][0], token) if redis_basket.exists?(session['warden.user.user.key'][0][0])
    session['warden.user.user.key'][0][0] = nil
    redirect_to '/login'
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

end