class CheckoutController < ApplicationController

  def new
    @products = @basket.all
    @order = Order.new
    @user = current_user || User.new
  end

  def create
    @order = ::CheckoutsService.new(session, order_params).make_order
    @basket.clear
    redirect_to '/'
  end

  private

  def order_params
    params.require(:order).permit(:user_id, :order_price, :dest_address, :name, :phone_number)
  end
end