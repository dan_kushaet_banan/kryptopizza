require 'spec_helper'
require 'rails_helper'

RSpec.describe BasketsService do
  describe '#add' do
    context 'when we add correct product' do
      let(:redis) { instance_double('Redis', exists?: false, set: true, get: [].to_json) }

      before do
        allow(Product).to receive(:find).with(1).and_return(OpenStruct.new(id: 1, name: 'Margarita'))
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'adds to the basket' do
        BasketsService.new({user_id: 1} ).add(1)
        expect(redis).to have_received(:set)
      end
    end

    context 'when we have no basket' do
      let(:redis) { instance_double('Redis', exists?: false, set: true, get: [].to_json) }

      before do
        allow(Product).to receive(:find).with(1).and_return(OpenStruct.new(id: 1, name: 'Margarita'))
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'returns the empty array' do
        BasketsService.new({user_id: 1} ).add(1)

        expect(redis).to have_received(:get).with(1).exactly(0).times
      end
    end

    context 'when we have a basket' do
      let(:redis) { instance_double('Redis', exists?: true, set: true, get: [{id: 2, name: 'Caucasian'}].to_json) }

      before do
        allow(Product).to receive(:find).with(1).and_return(OpenStruct.new(id: 1, name: 'Margarita'))
        allow(Product).to receive(:find).with(2).and_return(OpenStruct.new(id: 2, name: 'Caucasian'))
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'returns the array of hashes' do
        BasketsService.new({user_id: 1} ).add(1)
        expect(redis).to have_received(:get).with(1)
      end
    end
  end

  describe '#size' do
    context 'when the basket is empty' do
      let(:redis) { instance_double('Redis', exists?: false, set: true, get: [].to_json) }

      before do
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'returns size - 0' do
        size = BasketsService.new({ user_id: 1} ).size

        expect(size).to eq(0)
      end
    end
  end

  describe '#size' do
    context 'when the basket isn\'t empty' do
      let(:redis) { instance_double('Redis', exists?: true, set: true, get: [{id: 2, name: 'Caucasian'}, {id: 1, name: 'Margarita'}].to_json) }

      before do
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'returns size > 0' do
        size = BasketsService.new({ user_id: 1} ).size

        expect(size).to be > 0
      end
    end
  end

  describe '#clear' do
    context 'when basket is not nil' do
      let(:redis) { instance_double('Redis', del: 1, exists?: true, set: true, get: [{id: 2, name: 'Caucasian'}, {id: 1, name: 'Margarita'}].to_json) }

      before do
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'clears' do
        BasketsService.new({user_id: 1}).clear
        expect(redis).to have_received(:del).with(1).once
      end
    end

    context 'when basket is empty' do
      let(:redis) { instance_double('Redis', del: 0, exists?: false, set: true, get: [].to_json) }

      before do
        allow(Redis).to receive(:current).and_return(redis)
      end

      it 'clears too but returns 0' do
        expect(BasketsService.new({user_id: 1}).clear).to eq(0)
      end
    end
  end
end