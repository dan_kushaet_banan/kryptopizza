require 'spec_helper'
require 'rails_helper'

RSpec.describe AuthenticationController, type: :controller do
  describe 'GET #new' do

    it 'renders new.html.erb' do
      get :new
      expect(response).to render_template('new')
    end

    it 'makes new User' do
      allow(User).to receive(:new)
      get :new
      expect(User).to have_received(:new).once
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      let(:valid_params) do
        {
          user: {
            email: 'yanis@gmail.com',
            password: '5466'
          }
        }
      end

      let(:user) { instance_double('User', authenticate: true, id: 4) }

      before do
        allow(User).to receive(:find_by).with(id: nil).and_return(nil)
        allow(User).to receive(:find_by).with(email: valid_params[:user][:email]).and_return(user)
        allow(Redis.current).to receive(:exists?).with('token').and_return(false)
        allow(SecureRandom).to receive(:hex).and_return('token')
      end

      it 'checks the user email correction' do
        post 'create', params: valid_params
        expect(User.last.email).to eq(valid_params[:user][:email])
      end

      it 'has http status found' do
        post :create, params: valid_params
        expect(response).to have_http_status(302)
      end

      it 'authenticates user' do
        post :create, params: valid_params
        expect(user).to have_received(:authenticate).with(valid_params[:user][:password]).once
      end

      it 'makes session[\'warden.user.user.key\'][1] to user.id' do
        post :create, params: valid_params
        expect(session['warden.user.user.key'][0][0]).to eq(user.id)
      end

      it 'redirects to admin_user_path(user.id)' do
        post :create, params: valid_params
        expect(response).to redirect_to(admin_user_path(user.id))
      end
    end

    context 'with incorrect parameters' do
      let(:valid_params) do
        {
          user: {
            email: 'invalid@gmail.com',
            password: 'invalid'
          }
        }
      end

      let(:user) { instance_double('User', authenticate: false, id: 4) }

      before do
        allow(User).to receive(:find_by).with(id: nil).and_return(nil)
        allow(User).to receive(:find_by).with(email: valid_params[:user][:email]).and_return(user)
      end

      it 'renders #new page' do
        post :create, params: valid_params
        expect(response).to render_template('new')
      end

      it 'has http-status \'Unprocessable entity\' (422)' do
        post :create, params: valid_params
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'POST #destroy' do
    context 'when we have a basket' do
      let(:redis) { instance_double('Redis', rename: true, exists?: true, set: true, get: [{id: 2, name: 'Caucasian'}].to_json) }

      before do
        allow(Product).to receive(:find).with(2).and_return(OpenStruct.new(id: 2, name: 'Caucasian'))
        allow(Redis).to receive(:current).and_return(redis)
        allow(SecureRandom).to receive(:hex).and_return('token')
      end

      it 'renames the key of basket' do
        session['warden.user.user.key'][0][0] = 'id'
        post :destroy
        expect(redis).to have_received(:rename).with('id', 'token').once
      end

      it 'makes session[\'warden.user.user.key\'][1] to nil' do
        session['warden.user.user.key'][0][0] = 'id'
        post :destroy
        expect(session['warden.user.user.key'][0][0]).to eq(nil)
      end

      it 'redirects to /login' do
        post :destroy
        expect(response).to redirect_to('/login')
      end
    end

  end
end