require 'sidekiq/web'

Rails.application.routes.draw do

  devise_for :users, controllers: {registrations: 'registrations', sessions: 'sessions'}
  #devise_for :users, controllers: {sessions: 'sessions'}
  mount Sidekiq::Web => '/sidekiq'

  namespace :admin do
    resources :users do
      collection do
      end
    end

    resources :products do
      collection do
        post 'add/:id', to: 'products#add_to_basket'
        post 'delete/:id', to: 'products#delete_from_basket'
        get 'add/:id', to: 'products#index'
        get 'delete/:id', to: 'products#index'
      end
    end

    resources :orders do
      collection do
      end
    end

    resources :roles do
      collection do
      end
    end
  end

  get '/registration', to: 'registration#new'
  post '/registration', to: 'registration#registration'
  get '/login', to: 'authentication#new'
  post '/login', to: 'authentication#create'
  get '/logout', to: 'authentication#destroy'

  get '/basket', to: 'baskets#index'
  post '/basket/add/:id', to: 'baskets#add'
  post '/basket/delete/:id', to: 'baskets#remove'
  get '/basket/add/:id', to: 'baskets#index'
  get '/basket/delete/:id', to: 'baskets#index'
  post '/basket/clear', to: 'baskets#clear'

  get '/checkout', to: 'checkout#new'
  post '/checkout', to: 'checkout#create'

  get '/products/:id', to: 'products#show'
  post 'add/:id', to: 'products#add_to_basket'
  post 'delete/:id', to: 'products#delete_from_basket'
  get 'add/:id', to: 'products#index'
  get 'delete/:id', to: 'products#index'

  root 'products#index'
end
